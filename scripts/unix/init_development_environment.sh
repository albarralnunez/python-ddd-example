#!/bin/bash
echo "======================================================"
echo "This process will do some steps:"
echo "- Activate ./.venv virtual env in current directory."
echo "- Install requirements in virtualenv (mainly for IDE)."
echo "- Build project with PANTS."
echo "- Run project inside docker with a fake Virustotal service."
echo
echo "Press any key to continue, ctrl-C to exit."
echo "======================================================"
read -r
source .venv/bin/activate || (echo "Error activating virtualenv ./.venv. Please create it manually." && exit 1)
pip install -r 3rdparty/python/requirements.local.txt
