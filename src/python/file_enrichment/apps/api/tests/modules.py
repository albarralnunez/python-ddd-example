from injector import Module

from common.sql_alchemy.main.dependency_injection import SqlAlchemyModule
from file_enrichment.context.main.domain.enriched_file.enriched_file_repository_t import EnrichedFileRepositoryT
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.enriched_file_repository import \
    SqlAlchemyEnrichedFileRepository
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.mapping.enriched_signature_mapping import (
    EnrichedFileMapping,
)


class FileEnrichmentModule(Module):
    def configure(self, binder):
        binder.bind(EnrichedFileRepositoryT, to=SqlAlchemyEnrichedFileRepository)


modules = [
    SqlAlchemyModule(
        settings="file_enrichment/test/db_settings",
        mappings=[EnrichedFileMapping],
        testing=True,
    ),
    FileEnrichmentModule(),
]
