import json
from asyncio import AbstractEventLoop
from dataclasses import dataclass
from time import sleep

from aiokafka import AIOKafkaConsumer
from injector import Injector
from sqlalchemy.orm import Session

from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from common.shared_kernel.main.infrastructure.aio_kafka.aiokafka_settings import (
    AiokafkaSettings,
)
from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from common.utils.main.asyncio import force_async
from file_enrichment.context.main.application.file_signature_analysis_service import (
    FileSignatureAnalysisService,
)
from file_enrichment.context.main.domain.enriched_file.signature import Signature


@dataclass
class HashCalculatedConsumer:
    injector: Injector
    loop: AbstractEventLoop

    def config(self):
        mapping = self.injector.get(SqlAlchemyMapper)
        mapping.load()

    @force_async
    def close(self, session):
        session.commit()
        session.close()

    async def consume(self):
        aiokafka_settings = self.injector.get(AiokafkaSettings)

        consumer = AIOKafkaConsumer(
            aiokafka_settings.topic,
            loop=self.loop,
            bootstrap_servers=aiokafka_settings.url,
            group_id="HashCalculatedConsumer",
            auto_commit_interval_ms=1000,
            retry_backoff_ms=1000
        )

        await consumer.start()

        interval = 30

        try:
            async for msg in consumer:
                print(
                    "consumed: ",
                    msg.topic,
                    msg.partition,
                    msg.offset,
                    msg.key,
                    msg.value,
                    msg.timestamp,
                )
                data = json.loads(msg.value)

                file_analysis = self.injector.get(FileSignatureAnalysisService)
                signature = Signature(data["md5"])
                await file_analysis(signature)

                session = self.injector.get(Session)
                event_publisher = self.injector.get(EventPublisherT)
                await self.close(session)
                await event_publisher.flush()

        except Exception:
            raise
        finally:
            # Will leave consumer group; perform autocommit if enabled.
            await consumer.stop()
