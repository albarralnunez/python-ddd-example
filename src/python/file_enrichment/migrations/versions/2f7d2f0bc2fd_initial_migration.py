"""initial migration

Revision ID: 2f7d2f0bc2fd
Revises: 
Create Date: 2019-09-27 14:51:21.214783

"""
from enum import Enum

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.

revision = "2f7d2f0bc2fd"
down_revision = None
branch_labels = None
depends_on = None


class Status(Enum):
    DONE = 0
    PENDING = 1
    ERROR = 2


def upgrade():
    op.create_table(
        "enriched_file",
        sa.Column("signature", sa.Text(), primary_key=True),
        sa.Column("status", sa.Enum(Status), nullable=False),
        sa.Column("report", sa.Text(), nullable=False),
    )


def downgrade():
    op.delete_table("enriched_file")
