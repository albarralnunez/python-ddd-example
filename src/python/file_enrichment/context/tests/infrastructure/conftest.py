import pytest
from injector import Injector
from sqlalchemy.engine import Engine

from common.sql_alchemy.main.alembic import Alembic
from common.sql_alchemy.main.dependency_injection import SqlAlchemyModule
from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.mapping.enriched_signature_mapping import (
    EnrichedFileMapping,
)
from file_enrichment.migration_tool import migrate

modules = [
    SqlAlchemyModule(
        settings="file_enrichment/test/db_settings",
        mappings=[EnrichedFileMapping],
        testing=True,
    )
]


@pytest.fixture(scope="session")
def injector():
    injector = Injector(modules=modules)
    yield injector


@pytest.fixture(scope="session")
def setup_db(injector):
    engine = injector.get(Engine)
    mapper = injector.get(SqlAlchemyMapper)
    mapper.load()
    alembic = Alembic(engine)
    migrate(alembic)
    yield engine
    alembic.drop_db()
