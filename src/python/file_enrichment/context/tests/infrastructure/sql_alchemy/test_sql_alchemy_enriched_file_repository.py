import pytest
from hamcrest import empty, has_length
from hamcrest.core import assert_that
from hamcrest.core.core import is_
from sqlalchemy.orm import Session

from file_enrichment.context.main.infrastructure.sqlalchemy.file_enrichment.enriched_file_repository import (
    SqlAlchemyEnrichedFileRepository,
)
from file_enrichment.context.tests.mothers.signature_mother import SignatureMother
from file_enrichment.context.tests.mothers.enriched_file_mother import (
    EnrichedFileMother,
)


@pytest.fixture
def sql_alchemy_enriched_file_repository(setup_db):
    session = Session()
    repository = SqlAlchemyEnrichedFileRepository(session)
    yield repository
    session.rollback()
    session.close()


@pytest.mark.asyncio
async def test_sql_alchemy_enriched_file_repository_get_by_non_existing_id(
    sql_alchemy_enriched_file_repository
):
    signature = SignatureMother()
    aggregate = await sql_alchemy_enriched_file_repository.get_by_id(signature)
    assert_that(aggregate, is_(None))


@pytest.mark.asyncio
async def test_sql_alchemy_enriched_file_get_by_existing_id(
    sql_alchemy_enriched_file_repository
):
    aggregate_expected = EnrichedFileMother()
    await sql_alchemy_enriched_file_repository.add(aggregate_expected)
    aggregate_retrieved = await sql_alchemy_enriched_file_repository.get_by_id(
        aggregate_expected.signature
    )
    assert_that(aggregate_retrieved, is_(aggregate_expected))


@pytest.mark.asyncio
async def test_sql_alchemy_enriched_file_all(sql_alchemy_enriched_file_repository):
    aggregate_expected = await sql_alchemy_enriched_file_repository.get_all()
    assert_that(aggregate_expected, is_(empty()))


@pytest.mark.asyncio
async def test_sql_alchemy_enriched_file_save_multiple(
    sql_alchemy_enriched_file_repository
):
    number_of_aggregates = 10
    aggregates = EnrichedFileMother.build_batch(number_of_aggregates)
    for aggregate in aggregates:
        await sql_alchemy_enriched_file_repository.add(aggregate)
    operators_retrieved = await sql_alchemy_enriched_file_repository.get_all()
    assert_that(operators_retrieved, has_length(number_of_aggregates))
