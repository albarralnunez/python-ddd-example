import pytest

from common.async_test_utils.main.mocks import AsyncMock
from file_enrichment.context.main.application.file_signature_analysis_service import (
    FileSignatureAnalysisService,
)
from file_enrichment.context.main.domain.enriched_file.status import Status
from file_enrichment.context.tests.mothers.enriched_file_mother import (
    EnrichedFileMother,
)
from file_enrichment.context.tests.mothers.signature_mother import SignatureMother


@pytest.mark.asyncio
async def test_signature_analysis_service():
    enrichment_repository = AsyncMock()
    enriched_file_repository = AsyncMock()
    enriched_file_repository.get_by_id.return_value = None
    event_publisher = AsyncMock()
    application_service = FileSignatureAnalysisService(
        enrichment_repository=enrichment_repository,
        enriched_file_repository=enriched_file_repository,
        event_publisher=event_publisher,
    )

    signature = SignatureMother()
    enriched_file = EnrichedFileMother(signature=signature, status=Status.DONE)
    await application_service(signature)

    enrichment_repository.get_report.assert_called_once_with(signature)
    enriched_file_repository.add.assert_called_once_with(enriched_file)
    event_publisher.publish.assert_called_once()
