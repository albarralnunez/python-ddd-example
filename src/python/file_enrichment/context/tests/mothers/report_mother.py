import factory

from file_enrichment.context.main.domain.enriched_file.report import Report


class ReportMother(factory.Factory):
    value = factory.Faker("paragraph")

    class Meta:
        model = Report
