import factory

from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.status import Status
from file_enrichment.context.tests.mothers.signature_mother import SignatureMother
from file_enrichment.context.tests.mothers.report_mother import ReportMother


class EnrichedFileMother(factory.Factory):
    signature = factory.SubFactory(SignatureMother)
    status = Status.DONE
    report = factory.SubFactory(ReportMother)

    class Meta:
        model = EnrichedFile
