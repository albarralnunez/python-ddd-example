from common.shared_kernel.main.domain.md5.testing.md5_mother import MD5Mother
from file_enrichment.context.main.domain.enriched_file.signature import Signature


class SignatureMother(MD5Mother):
    class Meta:
        model = Signature
