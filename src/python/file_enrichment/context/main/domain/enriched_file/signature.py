from dataclasses import dataclass

from common.shared_kernel.main.domain.md5.md5 import MD5


@dataclass(frozen=True)
class Signature(MD5):
    ...
