from dataclasses import dataclass
from typing import Optional

from injector import inject

from file_enrichment.context.main.domain.enriched_file.enriched_file import EnrichedFile
from file_enrichment.context.main.domain.enriched_file.enriched_file_repository_t import (
    EnrichedFileRepositoryT,
)
from file_enrichment.context.main.domain.enriched_file.signature import Signature


@inject
@dataclass
class EnrichedFileDetailService:
    enriched_file_repository: EnrichedFileRepositoryT

    async def __call__(self, signature: Signature) -> Optional[EnrichedFile]:
        enriched_file = await self.enriched_file_repository.get_by_id(signature)
        return enriched_file
