import functools
from functools import partial
from typing import Awaitable


def force_async(fn):
    """
    Turns a sync function to async function using threads
    """
    import asyncio

    @functools.wraps(fn)
    async def wrapper(*args, **kwargs) -> Awaitable:
        loop = asyncio.get_event_loop()

        packed_function = partial(fn, *args, **kwargs)
        return await loop.run_in_executor(None, packed_function)

    return wrapper
