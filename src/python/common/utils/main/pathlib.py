from pathlib import Path


def get_dir(file_path: Path) -> Path:
    return file_path.resolve().parent
