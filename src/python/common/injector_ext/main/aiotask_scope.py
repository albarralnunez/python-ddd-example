from contextvars import ContextVar
from typing import Any, Dict

from injector import InstanceProvider, Provider, Scope


class AioTaskScope(Scope):
    def __init__(self, *args, **kwargs):
        self.cache = {}
        super().__init__(*args, **kwargs)

    def cleanup(self):
        self.cache: Dict[Any, ContextVar[Provider]] = {}

    def configure(self):
        self.cache = {}

    def _get_or_set_context_var(self, key) -> ContextVar:
        context_var: ContextVar = self.cache.get(key)
        if context_var is None:
            context_var = ContextVar(key)
            self.cache[key] = context_var
        return context_var

    def _get_or_set_provider(self, context_var, provider) -> InstanceProvider:
        try:
            value: InstanceProvider = context_var.get()
        except LookupError:
            value = InstanceProvider(provider.get(self.injector))
            context_var.set(value)
        return value

    def get(self, key: Any, provider: Provider) -> InstanceProvider:
        id_key = repr(key)
        context_var = self._get_or_set_context_var(id_key)
        value = self._get_or_set_provider(context_var, provider)
        return value
