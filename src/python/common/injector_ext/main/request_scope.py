from injector import ScopeDecorator

from common.injector_ext.main.aiotask_scope import AioTaskScope

request = ScopeDecorator(AioTaskScope)
