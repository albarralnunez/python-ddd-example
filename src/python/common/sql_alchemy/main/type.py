from abc import abstractmethod

from sqlalchemy import Text, TypeDecorator

from common.shared_kernel.main.domain.md5.md5 import MD5
from common.shared_kernel.main.domain.uuid.uuid import UUID


class ValueObjectAbstractType(TypeDecorator):
    @property
    @abstractmethod
    def value_object_type(self):
        raise NotImplementedError


class ValueObjectStrType(ValueObjectAbstractType):
    impl = Text

    @property
    def value_object_type(self):
        return str

    @property
    def python_type(self):
        return str

    def process_literal_param(self, value, dialect):
        ...

    def process_bind_param(self, value, dialect):
        if value is None:
            return None
        return value.value

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self.value_object_type(value)


class UUIDType(ValueObjectStrType):
    value_object_type = UUID


class MD5Type(ValueObjectStrType):
    value_object_type = MD5
