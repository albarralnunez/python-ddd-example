from abc import ABCMeta, abstractmethod
from typing import List, no_type_check, Awaitable

from injector import inject
from sqlalchemy.orm import Query, Session

from common.shared_kernel.main.domain.aggregate.aggregate import AggregateRootA
from common.utils.main.asyncio import force_async


class SqlAlchemyRepositoryA(metaclass=ABCMeta):
    @inject
    def __init__(self, session: Session):
        self._session = session

    async def commit(self):
        self._session.commit()

    @property
    @abstractmethod
    def aggregate(self):
        ...

    @property
    def _query(self) -> Query:
        return self._session.query(self.aggregate)

    @no_type_check
    @force_async
    def delete(self, entity: AggregateRootA) -> Awaitable:
        self._session.delete(entity)
        self._session.flush()

    @no_type_check
    @force_async
    def persist(self, entity: AggregateRootA) -> Awaitable:
        self._session.add(entity)
        self._session.flush()

    @no_type_check
    @force_async
    def all(self) -> Awaitable[List[AggregateRootA]]:
        return self._query.all()
