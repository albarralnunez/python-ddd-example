from hamcrest import assert_that, equal_to, match_equality, not_

from common.shared_kernel.main.domain.event.testing.domain_event_mother import (
    DomainEventTMother,
)
from common.shared_kernel.main.domain.event.testing.similar_event_matcher import (
    SimilarEvent,
)


def tests_similar_event_matcher_eq():
    aggregate_id = "test-aggregate-id"
    event_1 = DomainEventTMother(aggregate_id=aggregate_id)
    event_2 = DomainEventTMother(aggregate_id=aggregate_id)
    assert_that(event_1.message_id, not_(equal_to(event_2.message_id)))
    assert_that(event_1, equal_to(match_equality(SimilarEvent(event_2))))
