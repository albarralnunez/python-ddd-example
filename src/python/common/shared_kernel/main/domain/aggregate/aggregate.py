from dataclasses import dataclass
from typing import List

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT


@dataclass
class AggregateRootA:
    def __post_init__(self):
        self._domain_events: List[DomainEventT] = []

    def record(self, event: DomainEventT):
        self._domain_events.append(event)

    def pull_events(self):
        events = list(self._domain_events)
        self._domain_events.clear()
        return events
