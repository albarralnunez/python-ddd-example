import factory

from common.shared_kernel.main.domain.aggregate.aggregate import AggregateRootA
from common.shared_kernel.main.domain.uuid.testing.uuid_mother import UUIDMother


class AggregateRootAMother(factory.Factory):
    uid = factory.SubFactory(UUIDMother)

    class Meta:
        model = AggregateRootA
