import factory

from common.shared_kernel.main.domain.uuid.uuid import UUID


class UUIDMother(factory.Factory):
    value = factory.Faker("uuid4")

    class Meta:
        model = UUID
