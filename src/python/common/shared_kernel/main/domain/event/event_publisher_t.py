import abc
from abc import ABCMeta
from typing import Sequence

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT


class EventPublisherT(metaclass=ABCMeta):
    @abc.abstractmethod
    async def publish(self, event: Sequence[DomainEventT]):
        ...

    @abc.abstractmethod
    async def flush(self):
        ...

    @abc.abstractmethod
    async def clear(self):
        ...
