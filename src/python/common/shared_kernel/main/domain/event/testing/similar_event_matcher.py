from copy import copy
from typing import Dict

from hamcrest.core.base_matcher import BaseMatcher

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT


class SimilarEvent(BaseMatcher):
    def __init__(self, event):
        self.event: DomainEventT = event

    @staticmethod
    def _delete_message_id(message: Dict):
        del message["message_id"]

    def _get_value_list_with_out_message_id(self, obj):
        obj_dict = copy(obj.__dict__)
        self._delete_message_id(obj_dict)
        return sorted(obj_dict.items(), key=lambda x: x[0])

    def _matches(self, other_event: DomainEventT):
        copy_self = copy(self.event)
        copy_other = copy(other_event)
        values_self = self._get_value_list_with_out_message_id(copy_self)
        values_other = self._get_value_list_with_out_message_id(copy_other)
        return values_other == values_self

    def describe_to(self, description):
        description.append_text(self.event)
