import factory

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT


class DomainEventTMother(factory.Factory):
    aggregate_id = factory.Faker("pystr")

    class Meta:
        model = DomainEventT
