import factory

from common.shared_kernel.main.domain.md5.md5 import MD5


class MD5Mother(factory.Factory):
    value = factory.Faker("md5")

    class Meta:
        model = MD5
