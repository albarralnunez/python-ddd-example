import re
from dataclasses import dataclass


class InvalidMD5Length(ValueError):
    pass


class InvalidMD5Symbols(ValueError):
    pass


@dataclass(frozen=True)
class MD5:
    value: str

    def __post_init__(self):
        if len(self.value) != 32:
            raise InvalidMD5Length(f"The MD5 hash {self.value} has an invalid length")
        valid = re.findall(r"([a-f\d])", self.value)
        if len(valid) < 32:
            raise InvalidMD5Symbols(
                f"The MD5 hash {self.value} has invalid symbols (digits must be lowercase)"
            )
