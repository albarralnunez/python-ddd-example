from common.conf_settings.main.settings import BaseSettings


class AiokafkaSettings(BaseSettings):
    @property
    def mapping(self):
        return {"topic": str, "url": str}
