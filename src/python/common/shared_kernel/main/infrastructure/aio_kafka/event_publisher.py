import asyncio
import json
from typing import List, Sequence

from aiokafka import AIOKafkaProducer
from injector import inject

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT
from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from common.shared_kernel.main.infrastructure.aio_kafka.aiokafka_settings import (
    AiokafkaSettings,
)


class AioKafkaEventPublisherError(Exception):
    ...


class AiokafkaEventPublisher(EventPublisherT):
    @inject
    def __init__(self, settings: AiokafkaSettings):
        self.loop = asyncio.get_event_loop()
        self.producer = AIOKafkaProducer(
            loop=self.loop,
            bootstrap_servers=settings.url,
            value_serializer=self._serialize_domain_event,
            key_serializer=self._key_serializer,
        )
        self._cached_events: List[DomainEventT] = []
        self.settings = settings

    async def publish(self, events: Sequence[DomainEventT]):
        self._cached_events += events

    @staticmethod
    def _key_serializer(domain_event: DomainEventT):
        value = bytes(domain_event.aggregate_id, encoding="utf-8")
        return value

    @staticmethod
    def _serialize_domain_event(domain_event: DomainEventT):
        data = {}
        for attribute, _ in domain_event.__dataclass_fields__.items():
            value = getattr(domain_event, attribute)
            data.update({attribute: value})
        data.update({"message_type": domain_event.message_type})
        value = bytes(json.dumps(data), encoding="utf-8")
        return value

    async def flush(self):
        await self.producer.start()
        try:
            for event in self._cached_events:
                await self.producer.send(
                    topic=event.__class__.__name__, key=event, value=event
                )
        except Exception as err:
            import traceback

            traceback.print_tb(err.__traceback__)
        await self.clear()

    async def clear(self):
        self._cached_events = []
