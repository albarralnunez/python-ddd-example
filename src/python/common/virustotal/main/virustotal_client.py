from abc import ABCMeta
from dataclasses import dataclass

import aiohttp
from injector import inject

from common.shared_kernel.main.domain.md5.md5 import MD5
from common.virustotal.main.virustotal_settings import VirustotalSettings
from file_enrichment.context.main.domain.enriched_file.report import Report


@inject
@dataclass
class VirustotalClient(metaclass=ABCMeta):
    settings: VirustotalSettings

    async def get_report(self, md5: MD5) -> Report:
        api_key = self.settings.api_key
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"https://www.virustotal.com/vtapi/v2/file/report?apikey={api_key}&resource={md5.value}"
            ) as resp:
                report = Report(await resp.text())
        return report
