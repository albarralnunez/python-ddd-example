import asyncio

from injector import Injector

from file_repository.apps.minio_listener.main.minio_consumer import MinioListener
from file_repository.apps.minio_listener.main.modules import modules

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    injector = Injector(modules=modules)
    listener = MinioListener(injector, loop)
    listener.config()
    loop.run_until_complete(listener.consume())
