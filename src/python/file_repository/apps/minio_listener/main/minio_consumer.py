import json
from asyncio import AbstractEventLoop
from dataclasses import dataclass

from aiokafka import AIOKafkaConsumer
from injector import Injector
from sqlalchemy.orm import Session

from common.shared_kernel.main.domain.event.event_publisher_t import EventPublisherT
from common.shared_kernel.main.domain.uuid.uuid import UUID
from common.sql_alchemy.main.mapper import SqlAlchemyMapper
from common.utils.main.asyncio import force_async
from file_repository.context.main.application.file_registrar_service import (
    FileRegistrarService,
)
from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_name import FileName
from file_repository.context.main.domain.file.file_path import FilePath


@dataclass
class MinioListener:
    injector: Injector
    loop: AbstractEventLoop

    def config(self):
        mapping = self.injector.get(SqlAlchemyMapper)
        mapping.load()

    @force_async
    def close(self, session):
        session.commit()
        session.close()

    async def consume(self):
        consumer = AIOKafkaConsumer(
            "minio-topic",
            loop=self.loop,
            group_id="MinioListener",
            bootstrap_servers="kafka:9091",
            auto_commit_interval_ms=1000,
            retry_backoff_ms=1000
        )

        await consumer.start()

        try:
            async for msg in consumer:
                print(
                    "consumed: ",
                    msg.topic,
                    msg.partition,
                    msg.offset,
                    msg.key,
                    msg.value,
                    msg.timestamp,
                )
                data = json.loads(msg.value)

                if data["EventName"] != "s3:ObjectCreated:Put":
                    continue

                name_str = data["Records"][0]["s3"]["object"]["key"]
                id_ = FileID(UUID.random_str())
                name = FileName(name_str)
                path = FilePath(msg.key.decode())

                file_registrar = self.injector.get(FileRegistrarService)
                await file_registrar(id_=id_, name=name, path=path)

                session = self.injector.get(Session)
                event_publisher = self.injector.get(EventPublisherT)
                await self.close(session)
                await event_publisher.flush()

        except Exception:
            raise
        finally:
            # Will leave consumer group; perform autocommit if enabled.
            await consumer.stop()
