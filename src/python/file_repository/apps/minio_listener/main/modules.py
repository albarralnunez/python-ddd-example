from common.shared_kernel.main.infrastructure.aio_kafka.dependency_injection import (
    AiokafkaModule,
)
from common.sql_alchemy.main.dependency_injection import SqlAlchemyModule
from file_repository.context.main.infrastructure.dependency_injection import (
    FileInfoModule,
)
from file_repository.context.main.infrastructure.minio.dependency_injection import (
    MinioModule,
)
from file_repository.context.main.infrastructure.sql_alchemy.file_info.mapping.file_info_table_mapping import (
    FileInfoTableMapping,
)

modules = [
    SqlAlchemyModule(
        settings="file_repository/minio_listener/db_settings",
        mappings=[FileInfoTableMapping],
    ),
    AiokafkaModule(settings="file_repository/minio_listener/aiokafka_settings"),
    MinioModule(settings="file_repository/minio_listener/minio_storage"),
    FileInfoModule(),
]
