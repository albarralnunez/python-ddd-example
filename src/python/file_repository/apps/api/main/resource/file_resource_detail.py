from typing import Optional

from aiohttp import web
from sqlalchemy.orm import Session

from file_repository.apps.api.main.resource.resource import Resource
from file_repository.context.main.application.file_info_detail_retriever_service import (
    FileInfoDetailRetrieverService,
)
from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_info import FileInfo


class FileResourceDetail(Resource):
    async def get(self):
        application_service = self._injector.get(FileInfoDetailRetrieverService)
        session = self._injector.get(Session)

        id_str = self.request.match_info["id"]
        file_id = FileID(value=id_str)
        file_info: Optional[FileInfo] = await application_service(file_id)

        if not file_info:
            await self.close(session)
            return web.Response(text="Resource does not exist.", status=404)

        response = {
            "id": file_info.id_.value,
            "status": file_info.name.value,
            "report": file_info.path.value,
            "md5": file_info.md5.value,
        }

        await self.close(session)
        return web.json_response(response, status=200)
