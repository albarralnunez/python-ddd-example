from injector import Module

from common.sql_alchemy.main.dependency_injection import SqlAlchemyModule
from file_repository.context.main.domain.file.file_info_repository_t import FileInfoRepositoryT
from file_repository.context.main.infrastructure.sql_alchemy.file_info.file_info_repository import \
    SqlAlchemyFileInfoRepository
from file_repository.context.main.infrastructure.sql_alchemy.file_info.mapping.file_info_table_mapping import (
    FileInfoTableMapping,
)


class FileInfoModule(Module):
    def configure(self, binder):
        binder.bind(FileInfoRepositoryT, to=SqlAlchemyFileInfoRepository)


modules = [
    SqlAlchemyModule(
        settings="file_repository/test/db_settings",
        mappings=[FileInfoTableMapping],
        testing=True,
    ),
    FileInfoModule()
]
