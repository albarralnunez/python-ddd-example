import pytest

from common.async_test_utils.main.mocks import AsyncMock
from file_repository.context.main.application.file_registrar_service import (
    FileRegistrarService,
)
from file_repository.context.tests.mothers.file_id_mother import FileIDMother
from file_repository.context.tests.mothers.file_name_mother import FileNameMother
from file_repository.context.tests.mothers.file_path_mother import FilePathMother


@pytest.mark.asyncio
async def test_file_registrar_service_successfully():
    file_info_repository = AsyncMock()
    file_store_client = AsyncMock()
    event_publisher = AsyncMock()
    file_registrar_service = FileRegistrarService(
        file_info_repository=file_info_repository,
        file_store_client=file_store_client,
        event_publisher=event_publisher,
    )
    id_ = FileIDMother()
    name = FileNameMother()
    path = FilePathMother()
    await file_registrar_service(id_=id_, name=name, path=path)

    file_store_client.calculate_md5.assert_called_once()
    file_info_repository.add.assert_called_once()
    event_publisher.publish.assert_called_once()
