import factory

from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.tests.mothers.file_id_mother import FileIDMother
from file_repository.context.tests.mothers.file_name_mother import FileNameMother
from file_repository.context.tests.mothers.file_path_mother import FilePathMother


class FileInfoMother(factory.Factory):
    id_ = factory.SubFactory(FileIDMother)
    name = factory.SubFactory(FileNameMother)
    path = factory.SubFactory(FilePathMother)

    class Meta:
        model = FileInfo
