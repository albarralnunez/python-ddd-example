from common.shared_kernel.main.domain.uuid.testing.uuid_mother import UUIDMother
from file_repository.context.main.domain.file.file_id import FileID


class FileIDMother(UUIDMother):
    class Meta:
        model = FileID
