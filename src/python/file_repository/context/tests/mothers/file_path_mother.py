import factory

from file_repository.context.main.domain.file.file_path import FilePath


class FilePathMother(factory.Factory):
    value = factory.Faker("image_url")

    class Meta:
        model = FilePath
