import pytest
from hamcrest import empty, has_length
from hamcrest.core import assert_that
from hamcrest.core.core import is_
from sqlalchemy.orm import Session

from file_repository.context.main.infrastructure.sql_alchemy.file_info.file_info_repository import (
    SqlAlchemyFileInfoRepository,
)
from file_repository.context.tests.mothers.file_id_mother import FileIDMother
from file_repository.context.tests.mothers.file_info_mother import FileInfoMother


@pytest.fixture
def sql_alchemy_file_repository(setup_db, injector):
    session = injector.get(Session)
    repository = SqlAlchemyFileInfoRepository(session=session)
    yield repository
    session.rollback()


@pytest.mark.asyncio
async def test_sql_alchemy_file_repository_get_by_non_existing_id(
    sql_alchemy_file_repository
):
    id_ = FileIDMother()
    aggregate = await sql_alchemy_file_repository.get_by_id(id_)
    assert_that(aggregate, is_(None))


@pytest.mark.asyncio
async def test_sql_alchemy_file_repository_get_by_existing_id(
    sql_alchemy_file_repository
):
    aggregate_expected = FileInfoMother()
    await sql_alchemy_file_repository.add(aggregate_expected)
    aggregate_retrieved = await sql_alchemy_file_repository.get_by_id(
        aggregate_expected.id_
    )
    assert_that(aggregate_retrieved, is_(aggregate_expected))


@pytest.mark.asyncio
async def test_sql_alchemy_file_repository_all(sql_alchemy_file_repository):
    aggregate_expected = await sql_alchemy_file_repository.get_all()
    assert_that(aggregate_expected, is_(empty()))


@pytest.mark.asyncio
async def test_sql_alchemy_file_repository_save_multiple(sql_alchemy_file_repository):
    number_of_aggregates = 10
    aggregates = FileInfoMother.build_batch(number_of_aggregates)
    for aggregate in aggregates:
        await sql_alchemy_file_repository.add(aggregate)
    operators_retrieved = await sql_alchemy_file_repository.get_all()
    assert_that(operators_retrieved, has_length(number_of_aggregates))
