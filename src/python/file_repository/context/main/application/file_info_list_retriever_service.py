from dataclasses import dataclass
from typing import Iterator

from injector import inject

from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.infrastructure.sql_alchemy.file_info.file_info_repository import (
    SqlAlchemyFileInfoRepository,
)


@inject
@dataclass
class FileInfoListRetrieverService:
    file_info_repository: SqlAlchemyFileInfoRepository

    async def __call__(self) -> Iterator[FileInfo]:
        file_infos = await self.file_info_repository.get_all()
        return file_infos
