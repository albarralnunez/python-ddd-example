from dataclasses import dataclass

from common.shared_kernel.main.domain.event.domain_event_t import DomainEventT
from common.utils.main.no_default_attributes_dataclasses import NoDefaultVar, no_default


@dataclass(frozen=True)
class HashCalculatedDomainEvent(DomainEventT):
    md5: NoDefaultVar[str] = no_default
    sub_type: NoDefaultVar[str] = "hash_calculated"
    application: NoDefaultVar[str] = "file_repository"
