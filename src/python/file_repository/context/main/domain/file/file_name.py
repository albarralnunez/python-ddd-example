from dataclasses import dataclass


@dataclass(frozen=True)
class FileName:
    value: str
