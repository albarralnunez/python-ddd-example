from abc import ABCMeta, abstractmethod
from typing import Iterator, Optional

from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.domain.file.file_id import FileID


class FileInfoRepositoryT(metaclass=ABCMeta):
    @abstractmethod
    async def add(self, file: FileInfo):
        ...

    @abstractmethod
    async def get_all(self) -> Iterator[FileInfo]:
        ...

    @abstractmethod
    async def remove(self, file_info: FileInfo):
        ...

    @abstractmethod
    async def get_by_id(self, id_: FileID) -> Optional[FileInfo]:
        ...
