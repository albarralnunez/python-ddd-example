from dataclasses import dataclass
from typing import Optional

from common.shared_kernel.main.domain.aggregate.aggregate import AggregateRootA
from common.shared_kernel.main.domain.md5.md5 import MD5
from file_repository.context.main.contract.hash_calculated_domain_event import (
    HashCalculatedDomainEvent,
)
from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_name import FileName
from file_repository.context.main.domain.file.file_path import FilePath


@dataclass(eq=False)
class FileInfo(AggregateRootA):
    id_: FileID
    name: FileName
    path: FilePath
    md5: Optional[MD5] = None

    @classmethod
    def create(cls, id_: FileID, name: FileName, path: FilePath):
        file_info = cls(id_=id_, name=name, path=path)
        return file_info

    def add_m5(self, md5: MD5):
        self.md5 = md5
        domain_event: HashCalculatedDomainEvent = HashCalculatedDomainEvent(
            aggregate_id=self.id_.value, md5=md5.value
        )
        self.record(domain_event)
