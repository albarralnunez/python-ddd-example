from typing import Awaitable, no_type_check
from typing import Optional

from common.sql_alchemy.main.sqlalchemy_repository import SqlAlchemyRepositoryA
from common.utils.main.asyncio import force_async
from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.domain.file.file_id import FileID
from file_repository.context.main.domain.file.file_info_repository_t import (
    FileInfoRepositoryT,
)


class SqlAlchemyFileInfoRepository(SqlAlchemyRepositoryA, FileInfoRepositoryT):
    aggregate = FileInfo

    async def add(self, file_info: FileInfo):
        await self.persist(file_info)

    async def get_all(self):
        result = await self.all()
        return result

    async def remove(self, file_info: FileInfo):
        await self.delete(file_info)

    @no_type_check
    @force_async
    def get_by_id(self, id_: FileID) -> Awaitable[Optional[FileInfo]]:
        file_info = self._query.get(id_)
        return file_info
