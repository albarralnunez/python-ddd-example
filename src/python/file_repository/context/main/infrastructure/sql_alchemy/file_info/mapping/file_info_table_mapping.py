from sqlalchemy import Column, Table

from common.sql_alchemy.main.mapping import Mapping
from common.sql_alchemy.main.type import UUIDType, MD5Type
from file_repository.context.main.domain.file.file_info import FileInfo
from file_repository.context.main.infrastructure.sql_alchemy.file_info.mapping.type import (
    FileNameType,
    FilePathType,
)


class FileInfoTableMapping(Mapping):
    @property
    def table(self):
        return Table(
            "file_info",
            self._metadata,
            Column("id_", UUIDType(), primary_key=True),
            Column("name", FileNameType(), nullable=False),
            Column("path", FilePathType(), nullable=False),
            Column("md5", MD5Type(), nullable=True)
        )

    @property
    def entity(self):
        return FileInfo
