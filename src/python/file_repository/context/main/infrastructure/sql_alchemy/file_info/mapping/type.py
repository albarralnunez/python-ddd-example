from common.sql_alchemy.main.type import ValueObjectStrType
from file_repository.context.main.domain.file.file_name import FileName
from file_repository.context.main.domain.file.file_path import FilePath


class FileNameType(ValueObjectStrType):
    value_object_type = FileName


class FilePathType(ValueObjectStrType):
    value_object_type = FilePath
