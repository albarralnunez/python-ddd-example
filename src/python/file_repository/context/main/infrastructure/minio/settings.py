from typing import Mapping

from common.conf_settings.main.settings import BaseSettings


class MinioSettings(BaseSettings):
    @property
    def mapping(self) -> Mapping:
        return {"url": str, "aws_access_key_id": str, "aws_secret_access_key": str}
