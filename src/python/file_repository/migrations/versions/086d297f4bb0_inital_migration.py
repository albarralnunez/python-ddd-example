"""Inital migration

Revision ID: 086d297f4bb0
Revises: 
Create Date: 2019-07-16 15:43:01.320801

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "086d297f4bb0"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "file_info",
        sa.Column("id_", sa.Text(), primary_key=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("path", sa.Text(), nullable=False),
        sa.Column("md5", sa.Text(), nullable=True),
    )


def downgrade():
    op.delete_table("file")
