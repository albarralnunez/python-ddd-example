# Copyright 2019 Pants project contributors (see CONTRIBUTORS.md).
# Licensed under the Apache License, Version 2.0 (see LICENSE).

FROM centos:7

# Install a (more) modern gcc, a JDK, and dependencies for installing Python through Pyenv.
RUN yum -y update
RUN yum install -y centos-release-scl
RUN yum install -y \
        make \
        devtoolset-7-gcc{,-c++} \
        git \
        java-1.8.0-openjdk-devel \
        bzip2-devel \
        docker-client \
        libffi-devel \
        openssl-devel \
        readline-devel \
        sqlite-devel \
        zlib-devel \
        gcc

ENV PYENV_ROOT /pyenv-docker-build
ENV PYENV_BIN "${PYENV_ROOT}/bin/pyenv"
RUN git clone https://github.com/pyenv/pyenv ${PYENV_ROOT}

# NB: We intentionally do not use `--enable-shared`, as it results in our shipped wheels for the PEX release using
# `libpython.X.Y.so.1`, which means that the PEX will not work for any consumer interpreters that were statically
# built, i.e. compiled without `--enable-shared`. See https://github.com/pantsbuild/pants/pull/7794.

ARG PYTHON_27_VERSION=2.7.13
RUN /usr/bin/scl enable devtoolset-7 -- ${PYENV_BIN} install ${PYTHON_27_VERSION}

ARG PYTHON_37_VERSION=3.7.2
RUN /usr/bin/scl enable devtoolset-7 -- ${PYENV_BIN} install ${PYTHON_37_VERSION}

RUN ${PYENV_BIN} global ${PYTHON_27_VERSION} ${PYTHON_37_VERSION}
ENV PATH "${PYENV_ROOT}/shims:${PATH}"

WORKDIR /opt/project

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.4.0/wait /wait

COPY docker/pants/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh /wait

COPY 3rdparty/python/ /tmp/requirements

RUN pip3 install --upgrade pip  \
    && pip3 install -r /tmp/requirements/requirements.local.txt --pre \
    && rm -r /tmp/requirements

ENTRYPOINT ["/entrypoint.sh"]
