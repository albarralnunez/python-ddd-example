-r requirements.txt

# Code Quality
black
pytest
isort

boto3_type_annotations_with_docs
