BUILD ALL:
```bash
./pants binary src::
```

RUN:
```bash
./dist/file-repository-api.pex 
```

RUN TEST:
```bash
# ALL
./pants  test src::

# ...
./pants --tag=integration-test test src::
./pants --tag=unit-test test src::
```

LINTER:
```bash
./pants lint src::
```

INSTALL REQUIREMENTS LOCAL
```bash
 pip install -r 3rdparty/python/requirements.txt --pre
```